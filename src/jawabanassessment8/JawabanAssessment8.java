/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jawabanassessment8;

import FileIO.ObjectManager;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ZANDUT
 */
public class JawabanAssessment8
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        Scanner inputan = new Scanner(System.in);

        ArrayList<Departement> arrayDepartement = new ArrayList<>();
        ObjectManager io = new ObjectManager(arrayDepartement);

        int pil = -1;
        while (pil != 0)
        {
            System.out.println("====MENU====");
            System.out.println("1. Insert Departement");
            System.out.println("2. Insert Employee");
            System.out.println("3. Edit Departement by indeks");
            System.out.println("4. Edit Employee by indeks");
            System.out.println("5. Delete Departement by indeks");
            System.out.println("6. Delete Employee by indeks");
            System.out.println("7. View ALL");
            System.out.println("0. Exit");
            System.out.print("Masukkan pilihan : ");
            pil = inputan.nextInt();
            switch (pil)
            {
                case 1:
                    System.out.println("Masukkan Nama Departement ");
                    String nama = inputan.next();

                    //menambahkan objek departement ke arrayList
                    Departement depart = new Departement(nama);
                    arrayDepartement.add(depart);

                    //memasukkan arrayList dari departement ke file
                    if (io.saveObject(arrayDepartement))
                    {
                        System.out.println("Berhasil !!!");
                    }

                    break;
                case 2:
                    System.out.println("Masukkan indeks departement ");
                    int indeks = inputan.nextInt();
                    try
                    {
                        //kita ambil arrayList dari file
                        ArrayList<Departement> arrayDepart = (ArrayList<Departement>) io.readObject();
                        //ambil objek departement dari arrayList yg kita dapat by indeks
                        Departement newDepart = arrayDepart.get(indeks);

                        //inputan data employee
                        System.out.println("Masukkan ID employee (long) ");
                        long id = inputan.nextLong();
                        System.out.println("Masukkan Nama employee (String) ");
                        String name = inputan.next();
                        System.out.println("Masukkan Salary (Double) ");
                        double salary = inputan.nextDouble();

                        //membuat objek employee (inner class dari departemen)
                        Departement.Employee newEmployee = new Departement().new Employee(name, id, salary);
                        //add employee ke objek departement yg kita dapat
                        newDepart.addEmployee(newEmployee);

                        //update ke file
                        if (io.saveObject(arrayDepart))
                        {
                            System.out.println("Berhasil !!!");
                        }

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    System.out.println("Masukkan indeks departement ");
                    indeks = inputan.nextInt();
                    try
                    {
                        //kita ambil arrayList dari file
                        arrayDepartement = (ArrayList<Departement>) io.readObject();
                        //kita ambil objek departement dari arrayList by indeks
                        Departement newDepart = arrayDepartement.get(indeks);

                        //inputan data departement yg baru
                        System.out.println("Masukkan nama baru Departement ");
                        nama = inputan.next();

                        //setNama dari departement. Biar keganti dengan sebelumnya
                        newDepart.setName(nama);

                        //update ke arrayList by indeks dengan objek yang setelah di setNama yang baru
                        arrayDepartement.set(indeks, newDepart);

                        //update ke file
                        if (io.saveObject(arrayDepartement))
                        {
                            System.out.println("Berhasil !!!");
                        }

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    break;
                case 4:
                    System.out.println("Masukkan indeks departement ");
                    indeks = inputan.nextInt();
                    try
                    {
                        //ambil arrayList Departement dari file
                        arrayDepartement = (ArrayList<Departement>) io.readObject();
                        //ambil object departement dari arrayList by indeks
                        depart = arrayDepartement.get(indeks);

                        System.out.println("Masukkan indeks employee ");
                        int indeks1 = inputan.nextInt();

                        //mendapatkan employee berdasarkan indeks
                        Departement.Employee newEmployee = depart.getEmployee(indeks1);

                        //merubah data employee
                        System.out.println("Masukkan nama baru Employee ");
                        nama = inputan.next();
                        newEmployee.setName(nama);
                        System.out.println("Masukkan gaji baru Employee");
                        double salary = inputan.nextDouble();
                        newEmployee.setSalary(salary);

                        //update ke arrayList
                        arrayDepartement.set(indeks, depart);

                        //update ke file
                        if (io.saveObject(arrayDepartement))
                        {
                            System.out.println("Berhasil !!!");
                        }

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    System.out.println("Masukkan indeks departement ");
                    indeks = inputan.nextInt();
                    try
                    {
                        //mengambil arrayList dari file
                        arrayDepartement = (ArrayList<Departement>) io.readObject();
                        //mengambil objek departement by indeks dari arrayList
                        depart = arrayDepartement.get(indeks);

                        System.out.println("Yakin menghapus (*note Employee akan terhapus semua) ? (Y/N)  ");
                        String konfirmasi = inputan.next();
                        if (konfirmasi.equalsIgnoreCase("Y"))
                        {
                            //remove dari arrayList departement
                            arrayDepartement.remove(indeks);

                            //update File
                            if (io.saveObject(arrayDepartement))
                            {
                                System.out.println("Berhasil !!!");
                            }
                        }
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                case 6:
                    System.out.println("Masukkan indeks departement ");
                    indeks = inputan.nextInt();
                    try
                    {
                        //mengambil arrayList dari File
                        arrayDepartement = (ArrayList<Departement>) io.readObject();
                        //mengambil objek Departement dari arrayList by indeks
                        depart = arrayDepartement.get(indeks);

                        System.out.println("Masukkan indeks Employee ");
                        int indeks1 = inputan.nextInt();

                        System.out.println("Yakin menghapus ? (Y/N) ");
                        String konfirmasi = inputan.next();
                        if (konfirmasi.equalsIgnoreCase("Y"))
                        {
                            //remove employee by indeks dari Departement
                            depart.removeEmployee(indeks1);

                            //update ke arrayList
                            arrayDepartement.set(indeks, depart);

                            //update ke file
                            if (io.saveObject(arrayDepartement))
                            {
                                System.out.println("Berhasil !!!");
                            }
                        }

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    break;
                case 7:
                    //ambil arrayList dari File
                    ArrayList<Departement> array = (ArrayList<Departement>) io.readObject();
                    if (array.size() >= 1)
                    {
                        //iterasi dari ArrayList. display departement dan employee
                        for (Departement dep : array)
                        {
                            System.out.println("=====List Departement=====");
                            System.out.println("Nama Departement : " + dep.getName());

                            dep.showAllEmployee();
                        }
                    } else
                    {
                        System.out.println("Tidak ada Departement !!!");
                    }

                    break;
                case 0:

                    //menu terpenting. karena kalau tidak ada exit, program akan running terus. Maka dari itu, Exit penting beud.
                    System.out.println("Selamat jalan. Semoga selamat sampai tujuan !!");
                    break;

                default:

                    System.out.println("Tidak menu !!!");
                    break;
            }
        }

    }

}
